
const root = document.querySelector("#root");
const btn = document.querySelector("button");
const ip = "https://api.ipify.org/?format=json";
const address = "http://ip-api.com";


async function getIp() {
    await fetch(ip)
        .then((response) => response.json())
        .then((data) => {
            fetch(`${address}/json/${data.ip}`)
                .then((response) => response.json())
                .then((data) => renderAddress(data))
        })
};

function renderAddress({timezone, country, region, city, regionName}) {
    const ul = document.createElement("ul");
    const arr = [timezone, country, region, city, regionName]

    arr.forEach((el) => {
        const li = document.createElement("li");
        li.textContent = el;
        ul.append(li);
    });

    root.append(ul);
}


btn.addEventListener("click", (event) => {
    event.preventDefault();
    document.querySelectorAll("ul").forEach((el) => el.remove())
    getIp();
})